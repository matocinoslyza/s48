import React from 'react';
import {Row, Col, Button} from 'react-bootstrap';


export default function Banner() {
    return(
        <Row>
            <Col className="p-5">
                <h1 className="mb-3">My Course Booking App</h1>
                <p className="my-3">Opportunities for everyone, everywhere</p>
                <Button variant="primary">Enroll Now!</Button>
            </Col>
        </Row>
    )
};