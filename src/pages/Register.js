import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';

export default function Register() {
	const navigate = useNavigate();
	const { user } = useContext(UserContext);

	//State hooks to store the values of the input fields
	const [firstName, setfName] = useState('');
	const [lastName, setlName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	//State to determine whether submit button is enabled or not for conditional rendering
	const [isActive, setIsActive] = useState(true);


	useEffect(() => {
		//Validation to enable submit button when all fields are populated and both passwords match
		if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, password1, password2])


	function registerUser(e) {
        e.preventDefault();
        fetch('https://nameless-lake-45957.herokuapp.com/users/register', {
            method: 'POST',
			headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
                email: email,
				password1: password1,
				password2: password2
            })

        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            if(data){
                // Show a success message
                Swal.fire({
                    title: 'Success!',
                    icon: 'success',
                    text: 'Registered successfully'
                });
				navigate('/login')
                // Render the updated data using the fetchData prop
                // fetchData();
            }else{
                Swal.fire({
                    title: "Something went wrong",
                    icon: 'error',
                    text: 'Something went wrong. Please try again'
                })
                // fetchData();
            }
            // Clear out the input fields
			setfName('');
			setlName('');
			setEmail('');
			setPassword1('');
			setPassword2('');
        })
    }


	return (

		//Conditional rendering
		(user.accessToken !== null) ? 

		<Navigate to="/login" />

		:

		<Form onSubmit={(e) => registerUser(e)}>
			<h1>Register</h1>
			<Form.Group> 
				<Form.Label>First Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter First Name"
					required
					value={firstName}
					onChange={e => setfName(e.target.value)}
					/>
			</Form.Group>

			<Form.Group> 
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter Last Name"
					required
					value={lastName}
					onChange={e => setlName(e.target.value)}
					/>
			</Form.Group>

			<Form.Group> 
				<Form.Label>Email Adress</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter Email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else
					</Form.Text>
			</Form.Group>

			<Form.Group> 
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Enter Password"
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					/>
			</Form.Group>

			<Form.Group> 
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Verify Password"
					required
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					/>
			</Form.Group>

			{isActive ? 
				<Button variant="primary" type="submit" className="mt-3">Submit</Button>

				:

				<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>

			}
			
		</Form>
		)
}
